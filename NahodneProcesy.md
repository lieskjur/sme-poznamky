# Náhodný proces
Distribuční funkce
```math
F(x, t) = P\{\xi(t)<x\}
```
Hustota pravděpodobnosti
```math
f(x, t) = \frac{\partial F(x, t)}{\partial x}
```
Střední hodnota
```math
E(ξ(t)) = \int_{-∞}^{∞} x ⋅ f(x,t) \ dx = m_1(t)
```
Rozptyl
```math
D(ξ(t)) = \int_{-∞}^{∞} (x - m_1(t))^2 ⋅ f(x,t) \ dx
```
## Dvojice náhodných procesů
Distribuční funkce
```math
F\left(x, y, t_{1}, t_{2}\right)=P\left\{\xi^{(i)}\left(t_{1}\right) \leq x ; \quad \eta^{(i)}\left(t_{2}\right) \leq y\right\}
```
Hustota pravděpodobnosti
```math
f\left(x, y, t_{1}, t_{2}\right)=\frac{\partial^{2} F\left(x, y, t_{1}, t_{2}\right)}{\partial x \cdot \partial y}
```

### Korelace
Vzájemná korelace
```math
R_{xy}(t) ≡ m_{xy}(t) ≡ E(ξ(t),η(t)) = \int_{-∞}^{∞} \int_{-∞}^{∞} xy ⋅ f(x,y,t) dx dy
```
Autokorelace
```math
R_{xx}(τ) ≡ m_{xx}(τ) ≡ E(ξ(t),ξ(t+τ)) = \int_{-∞}^{∞} \int_{-∞}^{∞} xx' ⋅ f(x,x',t,t+τ) dx dx'
```
### Kovariace
Vzájemná kovariace
```math
C_{xy}(t) = \int_{-∞}^{∞} \int_{-∞}^{∞} (x-m_{1x}(t)) ⋅ (y-m_{1y}(t)) ⋅ f(x,y,t) dx dy
```
Autokovariace
```math
C_{xx}(τ) = \int_{-∞}^{∞} \int_{-∞}^{∞} (x-m_{1x}(t)) ⋅ (x'-m_{1x'}(t+τ)) ⋅ f(x,x',t,t+τ) dx dx'
```

## Ergodický náhodný proces
- statické vlastnosti stacionárního náhodného procesu se nemění v čase
```math
F(x,t) = F(x,t+τ)
\;,\quad 
f(x,t) = f(x,t+τ)
\;,\quad 
...
```
- ergodický náhodný proces je statický proces, jehož každou statistickou charakteristiku lze získat z libovolné realizace (za dostatečně dlouhou dobu)
```math
f(x,t) = f(x) = \text{konst}
\;,\quad 
p_i(t) = p_i = \text{konst}
```
---

Prvý obecný moment
```math
E \xi \equiv m_{1} \equiv \mu=\lim _{T \rightarrow \infty} \frac{1}{T} \int_{0}^{T} x(t) \cdot d t
```
Druhý centrální moment
```math
D \xi \equiv \mu_{2} \equiv \sigma^{2}=\lim _{T \rightarrow \infty} \frac{1}{T} \int_{0}^{T}\left(x(t)-m_{1}\right)^{2} d t=m_{2}-m_{1}^{2}
```
Autokorelace
```math
R_{x x}(\tau)=\lim _{T \rightarrow \infty} \frac{1}{T} \int_{0}^{T}(x(t) \cdot x(t+\tau)) d t
```
Autokovariace
```math
C_{x x}(\tau)=\lim _{T \rightarrow \infty} \frac{1}{T} \int_{0}^{T}\left(x(t)-m_{x x}\right) \cdot\left(x(t+\tau)-m_{x x}\right) d t
```