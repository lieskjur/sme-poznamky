# Náhodná Veličina

## Elementarni jev
```math
e_i ∈ \mathcal{E} \;,\quad \mathcal{E}=\left\{e_{1}, e_{2}, \ldots, e_{N}\right\}
```
## Náhodná veličina
```math
ξ = g(e)
```
## Množina náhodných jevů
```math
A=\{e: \xi \in(-\infty, x\rangle\}
```
## Pravděpodobnost
```math
P(A) \equiv P\{e:-\infty<\xi \leq x\} \equiv P\{-\infty<\xi \leq x\} \equiv P\{\xi \leq x\}=F(x) \\
P\left(\xi<x_{j}\right)=\sum_{i=1 \atop x<x_{j}}^{j} p_{i}=F\left(x_{j}\right)
```
## Hustota pravděpodobnosti
```math
f(x)=\frac{d F(x)}{d x} \text { resp. } F(x)=\int_{-\infty}^{x} f(x) d x
f(x_i) = p_i
```
## Momenty náhodné veličiny
```math
m_{k}=E \xi^{k}=\int_{-\infty}^{\infty} x^{k} f(x) d x \ldots \text { pro spojitou n.v., } \quad k=1,2, . . \\
m_{k} \equiv E \xi^{k}=\lim _{N \rightarrow \infty} \frac{1}{N} \sum_{i=1}^{N} x_{i}^{k} p_{i} \ldots \text { pro diskrétní n.v. }
```

## Centrální momenty náhodné veličiny
```math
\mu_{k}=E\left(\xi-m_{1}\right)^{k}=\int_{-\infty}^{\infty}\left(x-m_{1}\right)^{k} f(x) d x \ldots \text { pro spojitou n.v., } \quad k=1,2, \ldots \\
\mu_{k}=\lim _{N \rightarrow \infty} \frac{1}{N} \sum_{l}^{N}\left(x_{i}-m_{1}\right)^{k} p_{i} \ldots \text { pro diskrétní n.v. }
```

## Šikmost a Špičatost
```math
A^2=\frac{\mu_{3}^{2}}{\mu_{2}^{3}}
\;,\quad 
B=\frac{\mu_{4}}{\mu_{2}^{2}}-3
```
## Medián
```math
x_{\text{med}} : F(x_{\text{med}}) = 1/2
```
## Modus
```math
f(x_{\bmod })=\max \{f(x)\} ; \quad f^\prime (x_{\bmod })=0
```
## Momentová vytvořující funkce
```math
\begin{aligned}
M_{x}(t) ≡ E\left(e^{\xi t}\right) &=\int_{-\infty}^{\infty} e^{x t} f(x) d x \ldots \text { pro spojitou n.v. } \\
&=\sum_{i=1}^{N} p_{i} e^{x_{i} t} \ldots \text { pro diskrétní n.v. }
\end{aligned}
```
```math
m_{k}=\left[\frac{d^{k} M_{x}(t)}{d t^{k}}\right]_{t=0}
```