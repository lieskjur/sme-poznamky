# Úvod

## Diracův impulz
```math
f(t, a)
=
\left\{\begin{array}{l}
	0 \ldots t ∈ ( -a / 2, +a / 2 ) \\
	1 / a \ldots t ∉ ( -a / 2, +a / 2 )
\end{array}\right\}
```
## Konvoluce
```math
h(x)=f(x) * g(x)=\int_{-\infty}^{+\infty} f(u) \cdot g(x-u) d u
```
## Korelace
```math
h(x)=f(x) \star g(x)=\int_{-\infty}^{+\infty} f(u) \cdot g(u-x) d u
```