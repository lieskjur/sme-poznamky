# Průchod náhodného procesu lineární soustavou
```math
\begin{aligned}
Y(f) &=H(f) \cdot X(f) \\
y(t) &=h(\tau) * x(t)=\int_{-\infty}^{+\infty} h(\tau) x(t-\tau) d \tau
\end{aligned}
```
## Frekvenční přenos
```math
H(\omega) \equiv H(\omega, \zeta, \Omega)=\frac{Y(\omega)}{X(\omega)}=\frac{1}{1-\omega^{2} / \Omega^{2}+2 j \zeta \omega / \Omega}
```

## Frekvenční přenos a SPD
```math
\begin{aligned}
&H=\frac{Y}{X} \cdot \frac{Y^{*}}{Y^{*}}=\frac{|Y|^{2}}{X Y^{*}}=\frac{|Y|^{2}}{Y^{*} X}=\frac{S_{y y}}{S_{y x}} \\
&H=\frac{Y}{X} \cdot \frac{X^{*}}{X^{*}}=\frac{Y X^{*}}{|X|^{2}}=\frac{X^{*} Y}{|X|^{2}}=\frac{S_{x y}}{S_{x x}}
\end{aligned}
```
Dále lze odvodit
```math
S_{y y}=|H|^{2} S_{x x}
```

## Koherence
```math
\gamma^{2}=\frac{\left|S_{x y}\right|^{2}}{S_{x x} S_{y y}}
```
Pro lineární soustavy $`\gamma^{2}=1`$

## Cepstrum
Výsledek zpětné Fourierovy transformace grafu výkonové spektrální hustory
```math
C_{x x}(\tau): \log \left(S_{x x}(f)\right) \stackrel{F T^{-1}}{\rightarrow} C_{x x}(\tau)
```
Odkrývá vyšší harmonické frekvence v signálu (rychlý pokles jejich amplitud je kompenzován logaritmizací)

## Okénka
Mnohdy $`x(t) ≠ 0 `$ na okrajích intervalu $`t ∈ ⟨-T/2,T/2⟩`$, to může mít za výsledek chybé výsledky navazujících transformací. Proto se volí okénka $`w(t)`$ kterými se násobí signál aby $`x_w(-T/2) = x_w(T/2) = 0 \,, x_w(t) = x(t) ⋅ w(t)`$. Výsledný obraz je pak $`X_w(ω) = X(ω) * W(ω)`$.