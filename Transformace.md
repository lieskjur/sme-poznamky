# Fourierova transformace
## Spojitá
```math
X(f) = \mathcal{F}\{x(f)\} = \int_{-\infty}^{+\infty} x(t) \cdot e^{-j 2 \pi f t} dt \\
x(t) = \mathcal{F}^{-1}\{x(f)\} = \int_{-\infty}^{+\infty} X(f) \cdot e^{j 2 \pi f t} d f
```
## Diskrétní (DFT)
```math
X(k \cdot \Delta f)=  \mathcal{F}\{x(k \cdot \Delta f)\} = \Delta t \cdot \sum_{n=0}^{N-1} x(n \cdot \Delta t) e^{-j 2 \pi \frac{k n}{N}} \\
x(n \cdot \Delta t)=  \mathcal{F}^{-1}\{x(k \cdot \Delta f)\} = \Delta f \cdot \sum_{k=0}^{N-1} X(k \cdot \Delta f) e^{j 2 \pi \frac{k n}{N}}
```
<!-- ```math
x_n = x(n Δt)
\;,\quad 
X_k = X(k Δf)
\;,\quad 
Δf = \frac{1}{t_{max}} = \frac{1}{N} \frac{1}{Δt}
``` -->

## Vlastnosti
- linearita: $`ax(t) + by(t) \overset{\mathcal{F}}{↔} aX(f) + bY(f)`$
- podobnost: $`x(at) \overset{\mathcal{F}}{↔} \frac{1}{|a|} X(f/a)`$
- posun: $`x(t-a) \overset{\mathcal{F}}{↔} e^{-j2πfa} X(f)`$
- sudá funkce: $`x(t)|_\text{sudá} \overset{\mathcal{F}}{↔} X(f) = \phantom{x}_{Re} X(f) \,, \phantom{x}_{Im} X(f) = 0`$
- lichá funkce: $`x(t)|_\text{lichá} \overset{\mathcal{F}}{↔} X(f) = -j_{Im}X(f) \,, \phantom{x}_{Re}X(f) = 0`$
- opakování: $`x(t) \overset{\mathcal{F}}{→} X(f) \overset{\mathcal{F}}{→} x(-t) \overset{\mathcal{F}}{→} X(-f) \overset{\mathcal{F}}{→} x(t)`$
- komplexní sdružení: $`X(-f) = X^*(f)`$
- diracův impuls: $`δ(t) \overset{\mathcal{F}}{↔} 1`$
- Parselův vztah: $`\int_{-\infty}^{+\infty} x(t) \cdot y(t) \, dt = \int_{-\infty}^{+\infty} X^{*}(f) \cdot Y(f) \, df`$
- rovnost energie: $`\int_{-\infty}^{+\infty} x^{2}(t) d t=\int_{-\infty}^{+\infty}|X(f)|^{2} df`$
- konvoluce: $`x * y \overset{\mathcal{F}}{↔} XY \,, X * Y \overset{\mathcal{F}}{↔} xy`$

## FFT - Fast Fourier Transform
Rychlý algoritmus DFT pro $`N = 2^n`$ vzorků založený na rekurzivním rozkladě

# Hilbertova transformace
```math
\tilde{y}(t)=y(t) * \frac{1}{\pi t}=\int_{-\infty}^{+\infty} y(u) \frac{1}{\pi(t-u)} du
```
snažší výpočet ve frekvenčním
```math
\tilde{Y}(f)
=
Y(f) \cdot \int_{-\infty}^{+\infty} \frac{1}{\pi t} e^{-j 2 \pi f t} dt
=
Y(f) ⋅ ( - j ⋅ \operatorname{sign} f )
```
Obálka $`y(t)`$ je $`|z(t)|`$
```math
z(t)=y(t)+j \tilde{y}(t)
\;,\quad 
Z(f)=Y(f)+j \tilde{Y}(f)
```

## Výpočet
1. $`y(t) \overset{\mathcal{F}}{↔} Y(f)`$
2. $`f > 0: Z(f) = 2 Y(f)`$
3. $`Z(f) \overset{FT^{-1}}{↔} z(t)`$
4. $`|z(t)| = \sqrt{z \cdot z^*}`$
5. $`Φ(t) = arctan(\frac{ℑ z(t)}{ℜ z(t)})`$