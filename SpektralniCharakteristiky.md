# Spektrální charakteristiky

## Frekvenční spektrum
Fourierův obraz funkce času $`x(t)`$ na intervalu $`t ∈ ⟨-T/2,T/2⟩`$
```math
X_{T}(f)=\int_{-T / 2}^{+T / 2} x(t) e^{-j 2 \pi f t} \, dt
```
Periodogram
```math
\frac{1}{T} X_{T}^{*}(f) \cdot X_{T}(f)
```
## Výkonová spektrální hustota (PSD)
vzájemná
```math
S_{x y}(f)=\lim _{T \rightarrow \infty} E\left\{\frac{1}{T}\left(X_{T}^{*}(f) \cdot Y_{T}(f)\right)\right\}
```
auto
```math
S_{x x}(f)=\lim _{T \rightarrow \infty} E\left\{\frac{1}{T}\left(X_{T}^{*}(f) \cdot X_{T}(f)\right)\right\}
```
Pokud je proces ergodický, stačí jediný výběr (není třeba počítat střední hodnotu)

## Weiner-Chinčilovy vztahy
Vztahy mezi PSD a vzájemnou korelací (tvoří spolu "Fourierův pár")
```math
R_{x y}(\tau)=\int_{-\infty}^{+\infty} S_{x y}(f) e^{j 2 \pi f \tau} d f \\
S_{x y}(f)=\int_{-\infty}^{+\infty} R_{x y}(\tau) e^{-j 2 \pi f \tau} d \tau
```

### Bílý šum
```math
S_{xx}(ω) = \frac{N_0}{2} \overset{FT^{-1}}{\rightarrow} R_{xx}(τ) = \frac{N_0}{2} δ(τ)
```

## Jednostranné spektrum
```math
G_{x x}(f)=\left\{\begin{array}{ll}
2 S_{x x}(f) & \ldots f \geq 0 \\
0 & \ldots f<0
\end{array}\right\}
```

## Rozptyl
```math
σ_x^2 = \int_{-\infty}^{\infty} S_{xx}(f) df
```